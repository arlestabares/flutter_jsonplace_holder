import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_json_placeholder/src/app/domain/usecases/delete_posts_by_id_usecase.dart';
import 'package:flutter_json_placeholder/src/app/domain/usecases/usecases.dart';

import '../../core/injection/injection_container.dart';
import '../app/domain/usecases/find_all_favorites_posts_usecase.dart';
import '../app/presentation/bloc/zemoga_bloc.dart';

class MultiBlocProviderWidget extends StatelessWidget {
  const MultiBlocProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ZemogaBloc>(
          create: (context) => ZemogaBloc(
            findPostsUseCase: sl.get<FindPostsUseCase>(),
            getUserByIdUseCase: sl.get<FindUserByIdUseCase>(),
            updatePostsListUseCase: sl.get<UpdatePostsListUseCase>(),
            addFavoritesPostsToDbUseCase:
                sl.get<AddFavoritesPostsToDbUseCase>(),
            deleteAllPostsUseCase: sl.get<DeleteAllPostsUseCase>(),
            findAllFavoritesPostsFromDbUsecase:
                sl.get<FindAllFavoritesPostsUseCase>(),
            deletePostsByIdUseCase: sl.get<DeletePostsByIdUseCase>(),
            deleteFavoritePostUseCase: sl(),
          ),
        ),
      ],
      child: child,
    );
  }
}
