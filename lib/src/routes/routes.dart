import 'package:flutter/material.dart';
import 'package:flutter_json_placeholder/src/app/presentation/pages/posts_page.dart';
import 'package:flutter_json_placeholder/src/app/presentation/pages/posts_details_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'json_place_holder_page': (_) => const PostsPage(),
  'posts_details_page': (_) => const PostsDetailsPage(),
};
