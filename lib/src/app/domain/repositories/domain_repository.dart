import 'package:dartz/dartz.dart';
import 'package:flutter_json_placeholder/core/errors/failure.dart';

import '../entities/entities.dart';

abstract class DomainRepository {
  Future<Either<Failure, List<UserDomain>>> findUserById(int userId);
  Future<Either<Failure, List<PostsDomain>>> findAllPosts();
  Future<void> deleteAllPostsFromDb(List<PostsDomain> postsDomainList);
  Future<void> deletePostsByidFromDb(int postsId);
  Future<void> deleteFavoritePostFromDb(PostsDomain deleteFavoritePost);
  Future<void> deleteAllFavoritesPostsFromDb(
      List<PostsDomain> favoritesPostsList);
  Future<void> updatePostsListFromDb(List<PostsDomain> updatePostsList);
  Future<void> insertFavoritePostToDb(PostsDomain insertFavoritePost);
  Future<List<PostsDomain>> findAllFavoritesPostsFromDb();
}
