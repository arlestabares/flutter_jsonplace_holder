import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class UserDomain extends Equatable {
  int? id;
  final String? name;
  final String? username;
  final String? email;
  final String? phone;
  final String? website;
 

  UserDomain({
    this.id,
    this.name,
    this.username,
    this.email,
    this.phone,
    this.website,
   
  });

  @override
  List<Object?> get props => [id, name, phone, email, website];
}
