import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class PostsDomain extends Equatable {
  final int? id;
  final int? userId;
  final String? title;
  final String? body;
  int? isfavorite;

  PostsDomain({
    this.id,
    this.userId,
    this.title,
    this.body,
    this.isfavorite = 0,
  });

  @override
  List<Object?> get props => [id, userId, title, body, isfavorite];
}
