import 'package:equatable/equatable.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

import '../../../../core/usecase/use_case_db.dart';
import '../repositories/domain_repository.dart';

class UpdatePostsListUseCase implements UseCaseCoreDb<void, UpdateDbParams> {
  final DomainRepository domainRepository;

  UpdatePostsListUseCase(this.domainRepository);

  @override
  Future<void> call(UpdateDbParams params) async {
    return await domainRepository.updatePostsListFromDb(params.updatePostList);
  }
}

class UpdateDbParams extends Equatable {
  final List<PostsDomain> updatePostList;

  const UpdateDbParams({required this.updatePostList});
  @override
  List<Object?> get props => [];
}
