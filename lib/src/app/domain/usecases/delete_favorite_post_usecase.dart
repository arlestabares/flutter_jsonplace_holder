import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

import '../../../../core/usecase/use_case_db.dart';
import '../repositories/domain_repository.dart';

class DeleteFavoritePostUseCase
    implements UseCaseCoreDb<void, DeleteFavoriteParams> {
  final DomainRepository domainRepository;

  DeleteFavoritePostUseCase(this.domainRepository);

  @override
  Future<void> call(DeleteFavoriteParams params) {
    return domainRepository.deleteFavoritePostFromDb(params.deleteFavoritePost);
  }
}

class DeleteFavoriteParams {
  final PostsDomain deleteFavoritePost;

  DeleteFavoriteParams(this.deleteFavoritePost);
}
