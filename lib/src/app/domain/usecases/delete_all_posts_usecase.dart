import 'package:equatable/equatable.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

import '../../../../core/usecase/use_case_db.dart';
import '../repositories/domain_repository.dart';

class DeleteAllPostsUseCase implements UseCaseCoreDb<void, DeleteAllPostsParam> {
  final DomainRepository domainRepository;

  DeleteAllPostsUseCase(this.domainRepository);
  @override
  Future<void> call(DeleteAllPostsParam params) {
    return domainRepository.deleteAllPostsFromDb(params.deletedPostsList);
  }
}

class DeleteAllPostsParam extends Equatable {
  final List<PostsDomain> deletedPostsList;

  const DeleteAllPostsParam({required this.deletedPostsList});
  @override
  List<Object?> get props => [];
}
