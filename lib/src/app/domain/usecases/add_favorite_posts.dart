import 'package:equatable/equatable.dart';

import '../../../../core/usecase/use_case_db.dart';
import '../entities/post_domain.dart';
import '../repositories/domain_repository.dart';

class AddFavoritesPostsToDbUseCase
    implements UseCaseCoreDb<void, AddFavoritesParams> {
  final DomainRepository domainRepository;

  AddFavoritesPostsToDbUseCase(this.domainRepository);

  @override
  Future<void> call(AddFavoritesParams param) async {
    return await domainRepository.insertFavoritePostToDb(param.insertFavoritePost);
  }
}

class AddFavoritesParams extends Equatable {
  final PostsDomain insertFavoritePost;

  const AddFavoritesParams({required this.insertFavoritePost});
  @override
  List<Object?> get props => [insertFavoritePost];
}
