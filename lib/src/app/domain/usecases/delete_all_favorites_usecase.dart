import 'package:equatable/equatable.dart';
import 'package:flutter_json_placeholder/core/usecase/use_case_db.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

import '../repositories/domain_repository.dart';

class DeleteAllFavoritesPostsUseCase
    implements UseCaseCoreDb<void, DeleteAllFavoritesParams> {
  final DomainRepository domainRepository;

  DeleteAllFavoritesPostsUseCase(this.domainRepository);
  @override
  Future<void> call(DeleteAllFavoritesParams params) {
    return domainRepository
        .deleteAllFavoritesPostsFromDb(params.favoritesPostsList);
  }
}

class DeleteAllFavoritesParams extends Equatable {
  final List<PostsDomain> favoritesPostsList;

  const DeleteAllFavoritesParams({required this.favoritesPostsList});
  @override
  List<Object?> get props => [favoritesPostsList];
}
