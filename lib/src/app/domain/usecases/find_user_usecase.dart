import 'package:equatable/equatable.dart';
import 'package:flutter_json_placeholder/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_json_placeholder/core/usecase/use_case.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

import '../repositories/domain_repository.dart';

class FindUserByIdUseCase implements IUseCaseCore<List<UserDomain>, Params> {
  final DomainRepository domainRepository;

  FindUserByIdUseCase(this.domainRepository);
  @override
  Future<Either<Failure, List<UserDomain>>> call(Params param) async {
    return await domainRepository.findUserById(param.userId);
  }
}

class Params extends Equatable {
  final int userId;

  const Params({required this.userId});

  @override
  List<Object?> get props => [userId];
}
