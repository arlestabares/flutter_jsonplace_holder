import 'package:flutter_json_placeholder/core/usecase/use_case.dart';
import 'package:flutter_json_placeholder/core/usecase/use_case_db.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

import '../repositories/domain_repository.dart';

class FindAllFavoritesPostsUseCase
    implements UseCaseCoreDb<List<PostsDomain>, NoParams> {
  final DomainRepository domainRepository;

  FindAllFavoritesPostsUseCase(this.domainRepository);
  @override
  Future<List<PostsDomain>> call(NoParams params) {
    return domainRepository.findAllFavoritesPostsFromDb();
  }
}
