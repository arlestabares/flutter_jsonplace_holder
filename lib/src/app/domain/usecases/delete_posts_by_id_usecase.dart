import 'package:equatable/equatable.dart';
import 'package:flutter_json_placeholder/core/usecase/use_case_db.dart';

import '../repositories/domain_repository.dart';

class DeletePostsByIdUseCase
    implements UseCaseCoreDb<void, DeletePostsByIdParams> {
  final DomainRepository domainRepository;

  DeletePostsByIdUseCase(this.domainRepository);
  @override
  Future<void> call(DeletePostsByIdParams params) async {
    return await domainRepository.deletePostsByidFromDb(params.postsId);
  }
}

class DeletePostsByIdParams extends Equatable {
  final int postsId;

  const DeletePostsByIdParams(this.postsId);
  @override
  List<Object?> get props => [postsId];
}
