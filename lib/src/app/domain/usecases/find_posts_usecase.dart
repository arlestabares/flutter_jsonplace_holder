import 'package:flutter_json_placeholder/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_json_placeholder/core/usecase/use_case.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';
import 'package:flutter_json_placeholder/src/app/domain/repositories/domain_repository.dart';

class FindPostsUseCase implements IUseCaseCore<List<PostsDomain>, NoParams> {
  final DomainRepository domainRepository;

  FindPostsUseCase(this.domainRepository);
  @override
  Future<Either<Failure, List<PostsDomain>>> call(NoParams param) async {
    return await domainRepository.findAllPosts();
  }
}
