import 'package:floor/floor.dart';

@entity
class UserEntity {
  @primaryKey
  final int? id;
  final String? name;
  final String? username;
  final String? email;
  final String? phone;
  final String? website;

  UserEntity({
    this.id,
    this.name,
    this.username,
    this.phone,
    this.email,
    this.website,
   
  });
}
