import 'package:floor/floor.dart';

@entity
class PostsEntity {
  @primaryKey
  final int? id;
  final int? userId;
  final String? title;
  final String? body;
  int? isfavorite;
   

  PostsEntity({
    this.id,
    this.userId,
    this.title,
    this.body,
    this.isfavorite = 0,
    
  });
}
