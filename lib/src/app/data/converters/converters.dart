import 'package:flutter_json_placeholder/src/app/data/models/models.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

import '../datasources/dto/dto.dart';

class Converters {
//
  UserDomain userDToToUserDomain(UserDto userDto) {
    return UserDomain(
      id: userDto.id,
      name: userDto.name,
      username: userDto.username,
      email: userDto.email,
      phone: userDto.phone,
      website: userDto.website,
    );
  }

  PostsDomain postDtoToPostsDomain(PostsDto postDto) {
    return PostsDomain(
      id: postDto.id,
      userId: postDto.userId,
      title: postDto.title,
      body: postDto.body,
    );
  }

  UserDomain userEntityToUserDomain(UserEntity userModel) {
    return UserDomain(
      id: userModel.id,
      name: userModel.name,
      username: userModel.username,
      email: userModel.email,
      phone: userModel.phone,
      website: userModel.website
    );
  }

  PostsDomain postsEntityToPostsDomain(PostsEntity postsModel) {
    return PostsDomain(
      id: postsModel.id,
      userId: postsModel.userId,
      title: postsModel.title,
      body: postsModel.body,
      isfavorite: postsModel.isfavorite,
    );
  }

  PostsEntity postsDomainToPostsEntity(PostsDomain postEntity) {
    return PostsEntity(
      id: postEntity.id,
      userId: postEntity.userId,
      title: postEntity.title,
      body: postEntity.body,
      isfavorite: postEntity.isfavorite,
    );
  }

  UserEntity userDomainToUserEntity(UserDomain userEntity) {
    return UserEntity(
      id: userEntity.id,
      name: userEntity.name,
      username: userEntity.username,
      email: userEntity.email,
      phone: userEntity.phone,
      website: userEntity.website
    );
  }
}
