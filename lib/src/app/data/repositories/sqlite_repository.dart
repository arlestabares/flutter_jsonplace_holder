
import '../datasources/dto/dto.dart';
import '../datasources/local/sqlite/database_helper.dart';
abstract class Repository {
  Future<int> insertUserDto(UserDto userDto);
  Future<int> insertPostDto(PostsDto postDto);
  Future<List<int>> insertPostDtoList(List<PostsDto> postDtoList);
  Future<List<UserDto>> getAllUserDto();
  Future<List<PostsDto>> getAllPost();
  Future<void> deleteUserDto(UserDto userDto);
  Future<void> deletePost(PostsDto postDto);
  Future init();
  void close();
}
class SqliteRepository extends Repository {
  final dbHelper = DatabaseHelper.instance;

  @override
  Future<int> insertUserDto(UserDto userDto) async {
    final id = await dbHelper.insertUserDto(userDto);
    userDto.id = id;
    return id;
  }

  @override
  Future<int> insertPostDto(PostsDto postDto) async {
    final id = await dbHelper.insertPostDto(postDto);
    postDto.id = id;
    return id;
  }

  @override
  Future<List<UserDto>> getAllUserDto() {
    return dbHelper.getAllUserDto();
  }

  @override
  Future<List<PostsDto>> getAllPost() {
    return dbHelper.getAllPost();
  }

  @override
  Future<void> deletePost(PostsDto postDto) {
    return dbHelper.deletePostDto(postDto);
  }

  @override
  Future<void> deleteUserDto(UserDto userDto) {
    return dbHelper.deleteUserDto(userDto);
  }

  @override
  Future<List<int>> insertPostDtoList(List<PostsDto> postDtoList) async {
    return Future(
      () async {
        if (postDtoList.isNotEmpty) {
          final postIntList = <int>[];
          await Future.forEach(postDtoList, (PostsDto postDto) async {
            final postId = await dbHelper.insertPostDto(postDto);
            postDto.id = postId;
            postIntList.add(postId);
          });
          return postIntList;
        } else {
          return Future.value(<int>[]);
        }
      },
    );
  }

  @override
  Future init() async {
    await dbHelper.getDatabase;
    return Future.value();
  }

  @override
  void close() {
    dbHelper.close();
  }
}
