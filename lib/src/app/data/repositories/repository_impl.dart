import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_json_placeholder/core/errors/exceptions.dart';
import 'package:flutter_json_placeholder/core/errors/failure.dart';
import 'package:flutter_json_placeholder/src/app/data/converters/converters.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/local_data_source.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/remote/remote_datasource.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/post_domain.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/user_domain.dart';

import '../../domain/repositories/domain_repository.dart';

class RepositoryImpl implements DomainRepository {
  final RemoteDataSource remoteDataSource;
  final LocalDataSource localDataSource;
  late Converters converters = Converters();
  RepositoryImpl({
    required this.localDataSource,
    required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<PostsDomain>>> findAllPosts() async {
    var localDataSourceList = await localDataSource.findAllPostsFromDb();
    var postsList = <PostsDomain>[];
    var isfavoriteList = <PostsDomain>[];
    bool isfavorite = false;

    try {
      if (localDataSourceList.isEmpty) {
        var remoteList = await remoteDataSource.getAllPost();
        remoteList.fold(
          (failure) => () {},
          (data) {
            postsList = data;
            localDataSourceList = data;
            localDataSource.inserPostsListToDb(data);
          },
        );
      } else {
        for (var element in localDataSourceList) {
          if (element.isfavorite == 1) {
            isfavorite = true;
            postsList.add(element);
          } else {
            isfavorite = false;
          }
        }
        if (isfavorite) {
          var remoteList = await remoteDataSource.getAllPost();
          remoteList.fold(
            (failure) => () {},
            (data) {
              for (var element in data) {
                if (!postsList.contains(element)) {
                  postsList.add(element);
                }
              }
              localDataSource.updatePostsListToDb(postsList);
            },
          );
        }
        postsList =localDataSourceList;
      }
      return Right(postsList);
    } on ServerException {
      return const Left(ServerFailure('Failed the Server'));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    }
  }

  @override
  Future<Either<Failure, List<UserDomain>>> findUserById(int userId) async {
    var userFromDb = await localDataSource.findAllUserFromDb();

    var userDomainList = <UserDomain>[];
    var userDomain = UserDomain();
    var userIdTemp = 0;
    try {
      if (userFromDb.isNotEmpty) {
        for (var element in userFromDb) {
          if (element.id == userId) {
            userIdTemp = element.id!;
            userDomain = element;
          }
        }
        if (userIdTemp == userId) {
          userDomainList.add(userDomain);
        } else {
          var response = await remoteDataSource.getUserById(userId);
          response.fold(
            (failure) => Future.value(),
            (data) {
              for (var element in data) {
                localDataSource.insertUserToDb(element);
              }

              userDomainList = data;
            },
          );
        }
      } else {
        var response = await remoteDataSource.getUserById(userId);
        response.fold(
          (failure) => Future.value(),
          (data) {
            for (var element in data) {
              localDataSource.insertUserToDb(element);
            }

            userDomainList = data;
          },
        );
      }
      return Right(userDomainList);
    } on ServerException {
      return const Left(ServerFailure('Failed the Server'));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    }
  }

  @override
  Future<void> updatePostsList(List<PostsDomain> updatePostsList) async {
    await localDataSource.updatePostsListToDb(updatePostsList);
    return Future.value();
  }

  @override
  Future<void> deleteAllPostsFromDb(List<PostsDomain> postsDomainList) {
    return localDataSource.deleteAllPostsFromDb(postsDomainList);
  }

  @override
  Future<void> deletePostsByidFromDb(int postsId) {
    return localDataSource.deletePostsByidFromDb(postsId);
  }

  @override
  Future<void> insertFavoritePostToDb(PostsDomain insertFavoritesPosts) async {
    await localDataSource.inserFavoritePostToDb(insertFavoritesPosts);
    return Future.value();
  }

  @override
  Future<List<PostsDomain>> findAllFavoritesPostsFromDb() {
    return localDataSource.findAllFavoritesPostsFromDb();
  }

  @override
  Future<void> deleteAllFavoritesPostsFromDb(
      List<PostsDomain> favoritesPostsList) {
    return localDataSource.deleteAllFavoritesPostsFromDb(favoritesPostsList);
  }

  @override
  Future<void> deleteFavoritePostFromDb(PostsDomain deleteFavoritePost) {
    return localDataSource.deleteFavoritePostFromDb(deleteFavoritePost);
  }

  @override
  Future<void> updatePostsListFromDb(List<PostsDomain> updatePostsList) async {
    await localDataSource.updatePostsListToDb(updatePostsList);
    return Future.value();
  }
}
