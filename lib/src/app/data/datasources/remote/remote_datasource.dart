import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_json_placeholder/core/errors/exceptions.dart';
import 'package:flutter_json_placeholder/core/errors/failure.dart';
import 'package:flutter_json_placeholder/src/app/data/converters/converters.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/remote/services/web_service_api.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

abstract class RemoteDataSource {
  Future<Either<Failure, List<PostsDomain>>> getAllPost();
  Future<Either<Failure, List<UserDomain>>> getUserById(int userId);
}

class RemoteDataSourceImpl implements RemoteDataSource {
  final WebServiceApi webServiceApi;
  late Converters converters = Converters();

  RemoteDataSourceImpl({required this.webServiceApi});
  @override
  Future<Either<Failure, List<UserDomain>>> getUserById(int userId) async {
    try {
      final response = await webServiceApi.getUserById(userId);
      var temp = <UserDomain>[];
      for (var element in response) {
        temp.add(converters.userDToToUserDomain(element));
      }

      return Right(temp);
    } on ServerException {
      return const Left(ServerFailure('Failed the Server'));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    }
  }

  @override
  Future<Either<Failure, List<PostsDomain>>> getAllPost() async {
    var postsDomainList = <PostsDomain>[];
    try {
      final data = await webServiceApi.getAllPosts();
      for (var element in data) {
        final post = element;
        postsDomainList.add(converters.postDtoToPostsDomain(post));
      }
      return Right(postsDomainList);
    } on ServerException {
      return const Left(ServerFailure('Failed the Server'));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    }
  }
}
