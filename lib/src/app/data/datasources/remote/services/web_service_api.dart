import 'dart:convert';

import 'package:flutter_json_placeholder/core/errors/exceptions.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/remote/services/url_paths.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/remote/services/service_api.dart';
import 'package:http/http.dart' as http;

import '../../dto/dto.dart';

class WebServiceApi implements ServiceApi {
  @override
  Future<List<UserDto>> getUserById(int userId) async {
    // final response = await http.get(Uri.https(url, '/users/5'));
    final response = await http.get(Uri.parse(UrlPaths.searchUser(userId)));
    if (response.statusCode == 200) {
      final decodeData = json.decode(response.body);
      final Iterable jsonToList = decodeData;
      var userDtoList = jsonToList.map((e) => UserDto.fromJson(e)).toList();
      return userDtoList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<PostsDto>> getAllPosts() async {
    // final response = await http.get(Uri.https(url, '/posts'));
    final response = await http.get(Uri.parse(UrlPaths.searchPosts()));
    if (response.statusCode == 200) {
      final decodeData = json.decode(response.body);
      final Iterable jsonToList = decodeData;
      var postsDtoList = jsonToList.map((e) => PostsDto.fromJson(e)).toList();
      return postsDtoList;
    } else {
      throw ServerException();
    }
  }
}
