import '../../dto/dto.dart';

abstract class ServiceApi {
  Future<List<UserDto>> getUserById(int userId);
  Future<List<PostsDto>> getAllPosts();
}
