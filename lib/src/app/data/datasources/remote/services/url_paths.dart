class UrlPaths {
  UrlPaths._constructorPrivate();
  static const String baseUrl = 'https://jsonplaceholder.typicode.com';
  static String searchUser(int userId) => '$baseUrl/users?id=$userId';
  static String searchPosts() => '$baseUrl/posts';
}
