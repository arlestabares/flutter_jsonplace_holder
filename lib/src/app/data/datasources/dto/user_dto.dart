import 'dart:convert';

UserDto userDtoFromJson(String str) => UserDto.fromJson(json.decode(str));
class UserDto {
  int? id;
  final String? name;
  final String? username;
  final String? email;
  final String? phone;
  final String? website;

  UserDto({
    this.id,
    this.name,
    this.username,
    this.phone,
    this.email,
    this.website,
  });

  factory UserDto.fromJson(Map<String, dynamic> json) => UserDto(
        id: json["id"],
        name: json["name"],
        username: json["username"],
        email: json["email"],
        phone: json["phone"],
        website: json["website"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "username": username,
        "email": email,
        "phone": phone,
        "website": website,
      };
}

