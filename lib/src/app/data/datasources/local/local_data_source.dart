import 'package:flutter_json_placeholder/src/app/data/converters/converters.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/floor/dao/post_dao.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/floor/dao/user_dao.dart';
import 'package:flutter_json_placeholder/src/app/data/models/post_entity.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';

abstract class LocalDataSource {
  Future<void> insertUserToDb(UserDomain userDomain);
  Future<void> inserPostsListToDb(List<PostsDomain> userPostsList);
  Future<void> updatePostsListToDb(List<PostsDomain> updatePostsList);
  Future<void> updatePostToDb(PostsDomain updatePost);
  Future<List<PostsDomain>> findAllPostsFromDb();
  Future<void> deleteAllPostsFromDb(List<PostsDomain> postsDomainList);
  Future<void> deleteFavoritePostFromDb(PostsDomain deleteFavoritePost);
  Future<void> deletePostsByidFromDb(int postsId);
  Future<void> deleteAllFavoritesPostsFromDb(
      List<PostsDomain> favoritesPostsList);
  Future<UserDomain> findUserByIdFromDb(int userId);
  Future<List<UserDomain>> findAllUserFromDb();
  Future<void> inserFavoritePostToDb(PostsDomain insertFavoritePost);
  Future<List<PostsDomain>> findAllFavoritesPostsFromDb();
}

class LocalDataSourceImpl implements LocalDataSource {
  final UserDao userDao;
  final PostDao postDao;
  late Converters converters = Converters();

  LocalDataSourceImpl({
    required this.userDao,
    required this.postDao,
  });

  @override
  Future<void> inserPostsListToDb(List<PostsDomain> userPostsList) async {
    var lista = <PostsEntity>[];
    for (var element in userPostsList) {
      lista.add(converters.postsDomainToPostsEntity(element));
    }
    await postDao.inserPostsListDao(lista);
    return Future.value();
  }

  @override
  Future<List<PostsDomain>> findAllPostsFromDb() async {
    var lista = await postDao.findAllPosts();
    var listaDomain = <PostsDomain>[];
    for (var element in lista) {
      listaDomain.add(converters.postsEntityToPostsDomain(element));
    }
    return listaDomain;
  }

  @override
  Future<void> updatePostsListToDb(List<PostsDomain> updatePostsList) async {
    var listToUpdate = <PostsEntity>[];
    for (var element in updatePostsList) {
      final postsDomain = element;
      listToUpdate.add(converters.postsDomainToPostsEntity(postsDomain));
    }
    await postDao.updatePostsListDao(listToUpdate);

    return Future.value();
  }

  @override
  Future<void> insertUserToDb(UserDomain userModel) async {
    await userDao.insertUser(converters.userDomainToUserEntity(userModel));
    return Future.value();
  }

  @override
  Future<UserDomain> findUserByIdFromDb(int userId) async {
    var user = await userDao.findUserById(userId);
    return converters.userEntityToUserDomain(user!);
  }

  @override
  Future<void> deleteAllPostsFromDb(List<PostsDomain> postsDomainList) async {
    var list = <PostsEntity>[];
    for (var element in postsDomainList) {
      list.add(converters.postsDomainToPostsEntity(element));
    }
    await postDao.deletePostsList(list);
    return Future.value();
  }

  @override
  Future<void> deletePostsByidFromDb(int postsId) async {
    await postDao.deletePostsByid(postsId);
    return Future.value();
  }

  @override
  Future<void> inserFavoritePostToDb(PostsDomain insertFavoritePost) {
    postDao.updatePostDao(
        converters.postsDomainToPostsEntity(insertFavoritePost));
    return Future.value();
  }

  @override
  Future<List<PostsDomain>> findAllFavoritesPostsFromDb() async {
    var listTemp = <PostsDomain>[];
    var result = await postDao.findAllFavoritesPosts();
    for (var element in result) {
      listTemp.add(converters.postsEntityToPostsDomain(element));
    }
    return listTemp;
  }

  @override
  Future<void> deleteAllFavoritesPostsFromDb(
      List<PostsDomain> favoritesPostsList) {
    var listTemp = <PostsEntity>[];
    for (var element in favoritesPostsList) {
      listTemp.add(converters.postsDomainToPostsEntity(element));
    }
    postDao.deleteAllFavoritesPosts(listTemp);
    return Future.value();
  }

  @override
  Future<List<UserDomain>> findAllUserFromDb() async {
    var listTemp = await userDao.findAllUser();
    var listToReturn = <UserDomain>[];
    for (var element in listTemp) {
      listToReturn.add(converters.userEntityToUserDomain(element!));
    }
    return listToReturn;
  }

  @override
  Future<void> deleteFavoritePostFromDb(PostsDomain deleteFavoritePost) {
    postDao.deleteFavoritePost(
        converters.postsDomainToPostsEntity(deleteFavoritePost));
    return Future.value();
  }

  @override
  Future<void> updatePostToDb(PostsDomain updatePost) {
    postDao.updatePostDao(converters.postsDomainToPostsEntity(updatePost));
    return Future.value();
  }
}
