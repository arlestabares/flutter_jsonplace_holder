// ignore: depend_on_referenced_packages
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqlbrite/sqlbrite.dart';
import 'package:synchronized/synchronized.dart';

import '../../dto/dto.dart';

class DatabaseHelper {
  static const _dataBaseName = 'DataBase.db';
  static const _databaseVersion = 1;

  static const userTable = 'User';
  static const postTable = 'Post';
  static const favoritesTable = 'Favorites';

  static late BriteDatabase _streamDatabase;
  static var lock = Lock();

  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

//solo existe una única referencia de la base de datos en toda la aplicación.
  static Database? _database;

  Future _onCreate(Database db, int version) async {
    await db.execute('''
        CREATE TABLE $userTable(
          id INTEGER PRIMARY KEY,
          name TEXT,
          phone TEXT,
          email TEXT
          )
        ''');

    await db.execute('''
        CREATE TABLE $postTable(
          id INTEGER PRIMARY KEY,
          userId INTEGER,
          title TEXT,
          body TEXT
          )
        ''');
  }

  //Abre la base de datos (y la crea si no existe)
  Future<Database> _initDatabase() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, _dataBaseName);
    Sqflite.setDebugModeOn(true);
    return openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future<Database> get getDatabase async {
    if (_database != null) return _database!;
    // Usar este objeto para evitar el acceso simultáneo a los datos
    await lock.synchronized(
      () async {
        //crea una instancia perezosa de la base de datos la primera vez
        //que se accede a ella
        if (_database == null) {
          _database = await _initDatabase();
          _streamDatabase = BriteDatabase(_database!);
        }
      },
    );
    return _database!;
  }

  Future<Database> get getStreamDatabase async {
    await getDatabase;
    return _streamDatabase;
  }

  Future<int> _insert(String table, Map<String, dynamic> row) async {
    final db = await instance.getDatabase;
    return db.insert(table, row, conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<int> insertUserDto(UserDto userDto) {
    return _insert(userTable, userDto.toJson());
  }

  Future<int> insertPostDto(PostsDto postDto) {
    return _insert(postTable, postDto.toJson());
  }

  Future<List<UserDto>> getAllUserDto() async {
    final db = await instance.getDatabase;
    final userMapList = await db.query(userTable);
    final userList = parserUserDto(userMapList);
    return userList;
  }

  Future<List<PostsDto>> getAllPost() async {
    final db = await instance.getDatabase;
    final postMapList = await db.query(postTable);
    final postList = parsePost(postMapList);
    return postList;
  }

  Future<int> _delete(String table, int id) async {
    final db = await instance.getStreamDatabase;
    return db.delete(userTable, where: '$id = ?', whereArgs: [id]);
  }

  Future<int> deletePostDto(PostsDto postDto) async {
    if (postDto.id != null) {
      return _delete(postTable, postDto.id!);
    } else {
      return Future.value(-1);
    }
  }

  // Future<int> deletePostDtoList(List<PostDto> postDtoList) {
  //   for (var postDto in postDtoList) {
  //     if (postDto.id != null) {
  //       _delete(postTable, postDto.id!);
  //     }
  //   }
  //   return Future.value(-1);
  // }

  Future<int> deleteUserDto(UserDto userDto) async {
    if (userDto.id != null) {
      return _delete(userTable, userDto.id!);
    } else {
      return Future.value(-1);
    }
  }

  List<UserDto> parserUserDto(List<Map<String, dynamic>> userDtoMapList) {
    final userDtoList = <UserDto>[];
    for (var userDtoMap in userDtoMapList) {
      final userDto = UserDto.fromJson(userDtoMap);
      userDtoList.add(userDto);
    }
    return userDtoList;
  }

  List<PostsDto> parsePost(List<Map<String, dynamic>> postMapList) {
    final postList = <PostsDto>[];
    for (var postMap in postMapList) {
      final post = PostsDto.fromJson(postMap);
      postList.add(post);
    }
    return postList;
  }

  void close() {
    _database?.close();
  }
}
