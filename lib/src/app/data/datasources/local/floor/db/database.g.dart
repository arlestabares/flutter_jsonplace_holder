// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  // ignore: library_private_types_in_public_api
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  // ignore: library_private_types_in_public_api
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  UserDao? _userDaoInstance;

  PostDao? _posDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `UserEntity` (`id` INTEGER, `name` TEXT, `username` TEXT, `email` TEXT, `phone` TEXT, `website` TEXT, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `PostsEntity` (`id` INTEGER, `userId` INTEGER, `title` TEXT, `body` TEXT, `isfavorite` INTEGER, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  UserDao get userDao {
    return _userDaoInstance ??= _$UserDao(database, changeListener);
  }

  @override
  PostDao get posDao {
    return _posDaoInstance ??= _$PostDao(database, changeListener);
  }
}

class _$UserDao extends UserDao {
  _$UserDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _userEntityInsertionAdapter = InsertionAdapter(
            database,
            'UserEntity',
            (UserEntity item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'username': item.username,
                  'email': item.email,
                  'phone': item.phone,
                  'website': item.website
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<UserEntity> _userEntityInsertionAdapter;

  @override
  Future<UserEntity?> findUserById(int id) async {
    return _queryAdapter.query('SELECT * FROM  UserEntity WHERE id= ?1',
        mapper: (Map<String, Object?> row) => UserEntity(
            id: row['id'] as int?,
            name: row['name'] as String?,
            username: row['username'] as String?,
            phone: row['phone'] as String?,
            email: row['email'] as String?,
            website: row['website'] as String?),
        arguments: [id]);
  }

  @override
  Future<List<UserEntity?>> findAllUser() async {
    return _queryAdapter.queryList('SELECT * FROM  UserEntity',
        mapper: (Map<String, Object?> row) => UserEntity(
            id: row['id'] as int?,
            name: row['name'] as String?,
            username: row['username'] as String?,
            phone: row['phone'] as String?,
            email: row['email'] as String?,
            website: row['website'] as String?));
  }

  @override
  Future<void> insertUser(UserEntity userEntity) async {
    await _userEntityInsertionAdapter.insert(
        userEntity, OnConflictStrategy.ignore);
  }
}

class _$PostDao extends PostDao {
  _$PostDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _postsEntityInsertionAdapter = InsertionAdapter(
            database,
            'PostsEntity',
            (PostsEntity item) => <String, Object?>{
                  'id': item.id,
                  'userId': item.userId,
                  'title': item.title,
                  'body': item.body,
                  'isfavorite': item.isfavorite
                }),
        _postsEntityUpdateAdapter = UpdateAdapter(
            database,
            'PostsEntity',
            ['id'],
            (PostsEntity item) => <String, Object?>{
                  'id': item.id,
                  'userId': item.userId,
                  'title': item.title,
                  'body': item.body,
                  'isfavorite': item.isfavorite
                }),
        _postsEntityDeletionAdapter = DeletionAdapter(
            database,
            'PostsEntity',
            ['id'],
            (PostsEntity item) => <String, Object?>{
                  'id': item.id,
                  'userId': item.userId,
                  'title': item.title,
                  'body': item.body,
                  'isfavorite': item.isfavorite
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<PostsEntity> _postsEntityInsertionAdapter;

  final UpdateAdapter<PostsEntity> _postsEntityUpdateAdapter;

  final DeletionAdapter<PostsEntity> _postsEntityDeletionAdapter;

  @override
  Future<List<PostsEntity>> findAllPosts() async {
    return _queryAdapter.queryList('SELECT * FROM PostsEntity',
        mapper: (Map<String, Object?> row) => PostsEntity(
            id: row['id'] as int?,
            userId: row['userId'] as int?,
            title: row['title'] as String?,
            body: row['body'] as String?,
            isfavorite: row['isfavorite'] as int?));
  }

  @override
  Future<void> deletePostsByid(int postsId) async {
    await _queryAdapter.queryNoReturn('SELECT * FROM PostsEntity WHERE id = ?1',
        arguments: [postsId]);
  }

  @override
  Future<List<PostsEntity>> findAllFavoritesPosts() async {
    return _queryAdapter.queryList('SELECT * FROM PostsEntity',
        mapper: (Map<String, Object?> row) => PostsEntity(
            id: row['id'] as int?,
            userId: row['userId'] as int?,
            title: row['title'] as String?,
            body: row['body'] as String?,
            isfavorite: row['isfavorite'] as int?));
  }

  @override
  Future<void> insertPostsDao(PostsEntity userPostsModel) async {
    await _postsEntityInsertionAdapter.insert(
        userPostsModel, OnConflictStrategy.replace);
  }

  @override
  Future<void> inserPostsListDao(List<PostsEntity> userPostsList) async {
    await _postsEntityInsertionAdapter.insertList(
        userPostsList, OnConflictStrategy.replace);
  }

  @override
  Future<void> insertFavoritesPosts(PostsEntity insertFavoritePost) async {
    await _postsEntityInsertionAdapter.insert(
        insertFavoritePost, OnConflictStrategy.abort);
  }

  @override
  Future<void> updatePostDao(PostsEntity updatePost) async {
    await _postsEntityUpdateAdapter.update(
        updatePost, OnConflictStrategy.abort);
  }

  @override
  Future<void> updatePostsListDao(List<PostsEntity> updatePostsList) async {
    await _postsEntityUpdateAdapter.updateList(
        updatePostsList, OnConflictStrategy.abort);
  }

  @override
  Future<void> deletePostsList(List<PostsEntity> postsEntityList) async {
    await _postsEntityDeletionAdapter.deleteList(postsEntityList);
  }

  @override
  Future<void> deleteAllFavoritesPosts(
      List<PostsEntity> postsEntityList) async {
    await _postsEntityDeletionAdapter.deleteList(postsEntityList);
  }

  @override
  Future<void> deleteFavoritePost(PostsEntity deletFavoritePostsEntity) async {
    await _postsEntityDeletionAdapter.delete(deletFavoritePostsEntity);
  }
}
