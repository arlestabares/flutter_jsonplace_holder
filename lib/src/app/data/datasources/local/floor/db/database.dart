import 'package:floor/floor.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/floor/dao/post_dao.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/floor/dao/user_dao.dart';
import 'package:flutter_json_placeholder/src/app/data/models/models.dart';

import 'dart:async';
import 'package:sqflite/sqflite.dart' as sqflite;
part 'database.g.dart';
@Database(version: 1, entities: [UserEntity, PostsEntity])
abstract class AppDatabase extends FloorDatabase {
  UserDao get userDao;
  PostDao get posDao;
}
