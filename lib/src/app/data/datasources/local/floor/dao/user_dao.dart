import 'package:floor/floor.dart';

import '../../../../models/user_entity.dart';

@dao
abstract class UserDao {
  @Insert(onConflict: OnConflictStrategy.ignore)
  Future<void> insertUser(UserEntity userEntity);

  @Query('SELECT * FROM  UserEntity WHERE id= :id')
  Future<UserEntity?> findUserById(int id);

  @Query('SELECT * FROM  UserEntity ')
  Future<List<UserEntity?>> findAllUser();
}
