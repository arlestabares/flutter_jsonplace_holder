import 'package:floor/floor.dart';

import '../../../../models/post_entity.dart';

@dao
abstract class PostDao {
  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertPostsDao(PostsEntity userPostsModel);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> inserPostsListDao(List<PostsEntity> userPostsList);

  @update
  Future<void> updatePostDao(PostsEntity updatePost);
  @update
  Future<void> updatePostsListDao(List<PostsEntity> updatePostsList);

  @Query("SELECT * FROM PostsEntity")
  Future<List<PostsEntity>> findAllPosts();

  @Query("SELECT * FROM PostsEntity WHERE id = :postsId ")
  Future<void> deletePostsByid(int postsId);

  @delete
  Future<void> deletePostsList(List<PostsEntity> postsEntityList);
  @delete
  Future<void> deleteAllFavoritesPosts(List<PostsEntity> postsEntityList);
  @delete
  Future<void> deleteFavoritePost(PostsEntity deletFavoritePostsEntity);

  @insert
  Future<void> insertFavoritesPosts(PostsEntity insertFavoritePost);

  @Query('SELECT * FROM PostsEntity')
  Future<List<PostsEntity>> findAllFavoritesPosts();
}
