import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_json_placeholder/core/usecase/use_case.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';
import 'package:flutter_json_placeholder/src/app/domain/usecases/delete_favorite_post_usecase.dart';

import '../../domain/entities/entities.dart';
import '../../domain/usecases/delete_posts_by_id_usecase.dart';
import '../../domain/usecases/find_all_favorites_posts_usecase.dart';
import '../../domain/usecases/usecases.dart';

part 'zemoga_event.dart';
part 'zemoga_state.dart';

class ZemogaBloc extends Bloc<ZemogaEvent, ZemogaState> {
  final FindPostsUseCase findPostsUseCase;
  final FindUserByIdUseCase getUserByIdUseCase;
  final UpdatePostsListUseCase updatePostsListUseCase;
  final AddFavoritesPostsToDbUseCase addFavoritesPostsToDbUseCase;
  final DeleteAllPostsUseCase deleteAllPostsUseCase;
  final FindAllFavoritesPostsUseCase findAllFavoritesPostsFromDbUsecase;
  final DeletePostsByIdUseCase deletePostsByIdUseCase;
  final DeleteFavoritePostUseCase deleteFavoritePostUseCase;
  ZemogaBloc({
    required this.findPostsUseCase,
    required this.getUserByIdUseCase,
    required this.deleteFavoritePostUseCase,
    required this.updatePostsListUseCase,
    required this.deleteAllPostsUseCase,
    required this.addFavoritesPostsToDbUseCase,
    required this.findAllFavoritesPostsFromDbUsecase,
    required this.deletePostsByIdUseCase,
  }) : super(initialState) {
    on<FindAllPostsEvent>(_onFindAllPostsEvent);
    on<FindUserByIdEvent>(_onFindUserByIdEvent);
    on<AddFavoritePostsToDbEvent>(_onAddFavoritePostsToDbEvent);
    on<DeleteAllPostsEvent>(_onDeleteAllPostsEvent);
    on<ShowAllFavoritesPostsFromDbEvent>(_onShowAllFavoritesPostsEvent);
    on<UpdatePostsListFromDbEvent>(_onUpdatePostsListToDbEvent);
    on<UpdatePostsListEvent>(_onUpdatePostsListEvent);
    on<DeleteCurrentPostsEvent>(_onDeleteCurrentPostEvent);
    on<DeleteFavoritePostDbEvent>(_onDeleteFavoritePostEvent);
  }
  static ZemogaState get initialState => const ZemogaInitialState(Model());

  _onFindAllPostsEvent(
      FindAllPostsEvent event, Emitter<ZemogaState> emit) async {
    if (state.model.postsList!.isEmpty) {
      final result = await findPostsUseCase.call(NoParams());
      result.fold((failure) {
        emit(
          ErrorGetAllPostsState(state.model),
        );
      }, (data) {
        var isFavoristListFromDb = <PostsDomain>[];
        var isDataFromRedList = <PostsDomain>[];

        for (var element in data) {
          if (element.isfavorite == 1) {
            isFavoristListFromDb.add(element);
          } else {
            isDataFromRedList.add(element);
          }
        }
        emit(UploadedFindAllPostsState(
            state.model.copyWith(postsList: isDataFromRedList)));
        emit(UploadedAllFavoritesPostsFromDbState(
            state.model.copyWith(favoritesPostsList: isFavoristListFromDb)));
      });
    } else {
      emit(
        UploadedFindAllPostsState(
          state.model.copyWith(postsList: state.model.postsList),
        ),
      );
    }
  }

  _onFindUserByIdEvent(
      FindUserByIdEvent event, Emitter<ZemogaState> emit) async {
    final result = await getUserByIdUseCase.call(Params(userId: event.userId));
    result.fold(
      (failure) => emit(
        ErrorGetUserByIdState(state.model),
      ),
      (data) {
        var temp = UserDomain();
        for (var element in data) {
          temp = element;
        }
        emit(
          UploadedGetUserByIdState(
            state.model.copyWith(userDomain: temp),
          ),
        );
      },
    );
  }

  _onAddFavoritePostsToDbEvent(
      AddFavoritePostsToDbEvent event, Emitter<ZemogaState> emit) async {
    await addFavoritesPostsToDbUseCase
        .call(AddFavoritesParams(insertFavoritePost: event.addFavorites!));
    state.model.favoritesPostsList!.add(event.addFavorites!);
    emit(AddedToDbFavoritePostsState(state.model
        .copyWith(favoritesPostsList: state.model.favoritesPostsList)));
  }

  _onUpdatePostsListToDbEvent(
      UpdatePostsListFromDbEvent event, Emitter<ZemogaState> emit) async {
    await updatePostsListUseCase
        .call(UpdateDbParams(updatePostList: event.updatePostsDbList));

    UpdatedPostsListFromDbState(state.model);
  }

  _onDeleteAllPostsEvent(
      DeleteAllPostsEvent event, Emitter<ZemogaState> emit) async {
    var isNotFavoritePostList = <PostsDomain>[];
    if (state.model.postsList!.isNotEmpty) {
      for (var element in state.model.postsList!) {
        if (element.isfavorite != 1) {
          isNotFavoritePostList.add(element);
        }
      }
      state.model.postsList!.clear();
      await deleteAllPostsUseCase.call(
        DeleteAllPostsParam(deletedPostsList: isNotFavoritePostList),
      );
      emit(DeletedPostsListState(
          state.model.copyWith(postsList: state.model.postsList)));
    } else {
      emit(DeletedPostsListState(state.model));
    }
  }

  _onShowAllFavoritesPostsEvent(
      ShowAllFavoritesPostsFromDbEvent event, Emitter<ZemogaState> emit) async {
    var favoritesList = state.model.favoritesPostsList;
    var tempFavoritesList = <PostsDomain>[];
    if (favoritesList!.isNotEmpty) {
      for (var element in favoritesList) {
        if (element.isfavorite == 1) {
          tempFavoritesList.add(element);
        }
      }
    }
    emit(UploadedAllFavoritesPostsFromDbState(
        state.model.copyWith(favoritesPostsList: tempFavoritesList)));
  }

  _onUpdatePostsListEvent(
      UpdatePostsListEvent event, Emitter<ZemogaState> emit) async {
    emit(UpdatedPostsListState(state.model));
  }

  _onDeleteCurrentPostEvent(
      DeleteCurrentPostsEvent event, Emitter<ZemogaState> emit) async {
    if (event.deleteToPost.isfavorite == 1) {
      state.model.postsList!.remove(event.deleteToPost);
      emit(DeletedCurrentPostDbState(
          state.model.copyWith(postsList: state.model.postsList!)));
      return;
    } else {
      await deletePostsByIdUseCase
          .call(DeletePostsByIdParams(event.deleteToPost.id!));
      var temp = state.model.postsList!
          .firstWhere((element) => element.id == event.deleteToPost.id);
      state.model.postsList!.remove(temp);
      emit(UploadedFindAllPostsState(
          state.model.copyWith(postsList: state.model.postsList!)));
      emit(DeletedCurrentPostDbState(
          state.model.copyWith(postsList: state.model.postsList!)));
    }
  }

  _onDeleteFavoritePostEvent(
      DeleteFavoritePostDbEvent event, Emitter<ZemogaState> emit) async {
    await deleteFavoritePostUseCase
        .call(DeleteFavoriteParams(event.deleteFavoritePost));

    var postTemp = state.model.postsList!
        .firstWhere((element) => element.id == event.deleteFavoritePost.id);
    // for (var element in state.model.postsList!) {
    //   if (element.id == event.deleteFavoritePost.id) {
    //     state.model.postsList!.remove(element);
    //   }
    // }
        state.model.postsList!.remove(postTemp);
        // state.model.favoritesPostsList!.remove(postTemp);

    emit(
      DeletedCurrentPostDbState(
        state.model.copyWith(postsList: state.model.postsList!),
      ),
    );
    var temp = state.model.favoritesPostsList!
        .firstWhere((element) => element.id == event.deleteFavoritePost.id);
    state.model.favoritesPostsList!.remove(temp);

    emit(
      DeletedFavoritePostDbState(
        state.model.copyWith(postsList: state.model.postsList!),
      ),
    );
  }
}
