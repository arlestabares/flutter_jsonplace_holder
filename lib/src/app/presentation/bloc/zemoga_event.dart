part of 'zemoga_bloc.dart';

abstract class ZemogaEvent extends Equatable {
  const ZemogaEvent();

  @override
  List<Object> get props => [];
}

class ZemogaInitialEvent extends ZemogaEvent {}

class FindUserByIdEvent extends ZemogaEvent {
  final int userId;

  const FindUserByIdEvent(this.userId);
}

class FindAllPostsEvent extends ZemogaEvent {}

class AddFavoritePostsToDbEvent extends ZemogaEvent {
  final PostsDomain? addFavorites;

  const AddFavoritePostsToDbEvent({this.addFavorites});
}

class ShowAllFavoritesPostsFromDbEvent extends ZemogaEvent {}

class DeleteFavoritePostDbEvent extends ZemogaEvent {
  final PostsDomain deleteFavoritePost;

  const DeleteFavoritePostDbEvent(this.deleteFavoritePost);
}

class DeleteAllFavoritePostsDbEvent extends ZemogaEvent {}

class DeleteCurrentPostsEvent extends ZemogaEvent {
  final PostsDomain deleteToPost;

  const DeleteCurrentPostsEvent(this.deleteToPost);
}

class DeleteAllPostsEvent extends ZemogaEvent {}

class UpdatePostsEvent extends ZemogaEvent {}

class UpdatePostsListEvent extends ZemogaEvent {}

class UpdateFavoritesPostsEvent extends ZemogaEvent {
  final List<PostsDomain> updateFavoritesPostsList;

  const UpdateFavoritesPostsEvent({required this.updateFavoritesPostsList});
}

class UpdatePostsListFromDbEvent extends ZemogaEvent {
  final List<PostsDomain> updatePostsDbList;

  const UpdatePostsListFromDbEvent({required this.updatePostsDbList});
}
