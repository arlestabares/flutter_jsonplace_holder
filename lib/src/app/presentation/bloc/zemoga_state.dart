part of 'zemoga_bloc.dart';

abstract class ZemogaState extends Equatable {
  const ZemogaState(this.model);
  final Model model;

  @override
  List<Object> get props => [];
}

class ZemogaInitialState extends ZemogaState {
  const ZemogaInitialState(Model model) : super(model);
}

class UploadedGetUserByIdState extends ZemogaState {
  const UploadedGetUserByIdState(Model model) : super(model);
}

class UploadedFindAllPostsState extends ZemogaState {
  const UploadedFindAllPostsState(Model model) : super(model);
}

class UpdatedPostsListState extends ZemogaState {
  const UpdatedPostsListState(Model model) : super(model);
}

class AddedToDbFavoritePostsState extends ZemogaState {
  const AddedToDbFavoritePostsState(Model model) : super(model);
}

class UploadedAllFavoritesPostsFromDbState extends ZemogaState {
  const UploadedAllFavoritesPostsFromDbState(Model model) : super(model);
}

class DeletedFavoritePostDbState extends ZemogaState {
  const DeletedFavoritePostDbState(Model model) : super(model);
}
class DeletedAllFavoritesPostsDbState extends ZemogaState {
  const DeletedAllFavoritesPostsDbState(Model model) : super(model);
}

class DeletedCurrentPostDbState extends ZemogaState {
  const DeletedCurrentPostDbState(Model model) : super(model);
}

class DeletedPostsListState extends ZemogaState {
  const DeletedPostsListState(Model model) : super(model);
}

class UpdatedPostsListFromNetworkState extends ZemogaState {
  const UpdatedPostsListFromNetworkState(Model model) : super(model);
}

class UpdatedPostsListFromDbState extends ZemogaState {
  const UpdatedPostsListFromDbState(Model model) : super(model);
}

class ErrorGetAllPostsState extends ZemogaState {
  const ErrorGetAllPostsState(Model model) : super(model);
}

class ErrorGetUserByIdState extends ZemogaState {
  const ErrorGetUserByIdState(Model model) : super(model);
}

class ErrorAddingPostsToDbState extends ZemogaState {
  const ErrorAddingPostsToDbState(Model model) : super(model);
}

class ErrorUpdatingPostsListDbState extends ZemogaState {
  const ErrorUpdatingPostsListDbState(Model model) : super(model);
}

class Model extends Equatable {
  final int isFavorite;
  final UserDomain? userDomain;
  final List<UserDomain>? userList;
  final List<PostsDomain>? postsList;
  final List<PostsDomain>? favoritesPostsList;

  const Model({
    this.isFavorite = 0,
    this.userList = const <UserDomain>[],
    this.userDomain,
    this.postsList = const <PostsDomain>[],
    this.favoritesPostsList = const <PostsDomain>[],
  });

  Model copyWith({
    int? isFavorite,
    UserDomain? userDomain,
    List<UserDomain>? userList,
    List<PostsDomain>? postsList,
    List<PostsDomain>? favoritesPostsList,
  }) =>
      Model(
        isFavorite: isFavorite ?? this.isFavorite,
        userDomain: userDomain ?? this.userDomain,
        userList: userList ?? this.userList,
        postsList: postsList ?? this.postsList,
       favoritesPostsList: favoritesPostsList ?? this.favoritesPostsList
      );

  @override
  List<Object?> get props =>
      [isFavorite, userDomain, userList, postsList, favoritesPostsList];
}
