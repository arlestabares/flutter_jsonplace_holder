import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_json_placeholder/src/app/presentation/bloc/zemoga_bloc.dart';

class PostsList extends StatelessWidget {
  const PostsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZemogaBloc, ZemogaState>(
      buildWhen: (_, state) =>
          state is UploadedFindAllPostsState ||
          state is DeletedCurrentPostDbState ||
          state is AddedToDbFavoritePostsState ||
          state is DeletedCurrentPostDbState ||
          state is DeletedPostsListState,
      builder: (BuildContext context, state) {
        if (state is UploadedFindAllPostsState ||
            state is DeletedCurrentPostDbState ||
            state is AddedToDbFavoritePostsState ||
            state is DeletedCurrentPostDbState ||
            state is DeletedPostsListState) {
          if (state.model.postsList!.isEmpty) {
            return const Center(
              child: Text('Debe Actualizar para cargar los datos'),
            );
          }
          return ListView.builder(
            itemCount: state.model.postsList?.length,
            itemBuilder: (context, index) {
              final post = state.model.postsList?[index];

              return Dismissible(
                key: UniqueKey(),
                direction: DismissDirection.endToStart,
                background: Container(
                  color: Colors.red,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: const Icon(
                    Icons.delete_forever,
                  ),
                ),
                onDismissed: (DismissDirection direction) async {
                  context
                      .read<ZemogaBloc>()
                      .add(DeleteCurrentPostsEvent(post!));
                },
                child: Column(
                  children: [
                    ListTile(
                      leading: Center(
                        heightFactor: 10.0,
                        widthFactor: 0.0,
                        child: (post!.isfavorite! == 0)
                            ? const CircleAvatar(
                                backgroundColor: Colors.blue,
                                radius: 8.0,
                              )
                            : const Icon(
                                Icons.star_border_outlined,
                                color: Colors.yellow,
                              ),
                      ),
                      title: Text(
                        post.body!,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                      trailing: IconButton(
                        icon: const Icon(Icons.arrow_forward_ios),
                        onPressed: () async {
                          context.read<ZemogaBloc>().add(
                                FindUserByIdEvent(post.userId!),
                              );
                          Future.delayed(const Duration(seconds: 1));
                          Navigator.pushNamed(
                            context,
                            'posts_details_page',
                            arguments: post,
                          );
                        },
                      ),
                    ),
                    const Divider(height: 2.0, color: Colors.grey),
                  ],
                ),
              );
            },
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}

class FavoritesPostsList extends StatelessWidget {
  const FavoritesPostsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZemogaBloc, ZemogaState>(
      buildWhen: (_, state) =>
          state is UploadedAllFavoritesPostsFromDbState ||
          state is DeletedFavoritePostDbState,
      builder: (BuildContext context, state) {
        if (state is UploadedAllFavoritesPostsFromDbState ||
            state is DeletedFavoritePostDbState) {
          if (state.model.favoritesPostsList!.isEmpty) {
            return const Center(
              child: Text('No Favorites added'),
            );
          }
          return ListView.builder(
            itemCount: state.model.favoritesPostsList?.length,
            itemBuilder: (context, index) {
              final post = state.model.favoritesPostsList?[index];
              return Dismissible(
                key: UniqueKey(),
                direction: DismissDirection.endToStart,
                background: Container(
                  color: Colors.red,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: const Icon(
                    Icons.delete_forever,
                  ),
                ),
                onDismissed: (DismissDirection direction) async {
                  context
                      .read<ZemogaBloc>()
                      .add(DeleteFavoritePostDbEvent(post!));
                },
                child: Column(
                  children: [
                    ListTile(
                      leading: Center(
                          heightFactor: 10.0,
                          widthFactor: 0.0,
                          child: (post!.isfavorite! == 0)
                              ? const CircleAvatar(
                                  backgroundColor: Colors.blue,
                                  radius: 8.0,
                                )
                              : const Icon(
                                  Icons.star_border_outlined,
                                  color: Colors.yellow,
                                )),
                      title: Text(
                        post.body!,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                      trailing: IconButton(
                        icon: const Icon(Icons.arrow_forward_ios),
                        onPressed: () async {
                          context.read<ZemogaBloc>().add(
                                FindUserByIdEvent(post.userId!),
                              );
                          Future.delayed(const Duration(seconds: 1));
                          Navigator.pushNamed(
                            context,
                            'posts_details_page',
                            arguments: post,
                          );
                        },
                      ),
                    ),
                    const Divider(height: 2.0, color: Colors.grey),
                  ],
                ),
              );
            },
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
