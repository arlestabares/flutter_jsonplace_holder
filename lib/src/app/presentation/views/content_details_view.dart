import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_json_placeholder/src/app/domain/entities/entities.dart';
import 'package:flutter_json_placeholder/src/app/presentation/bloc/zemoga_bloc.dart';

class ContentDetailsView extends StatelessWidget {
  const ContentDetailsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PostsDomain post =
        ModalRoute.of(context)!.settings.arguments as PostsDomain;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text(
            'Description',
            style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w600),
          ),
          const SizedBox(height: 21.0),
          Text(
            post.title!,
            style: const TextStyle(fontSize: 16.0),
          ),
          const SizedBox(height: 21.0),
          const Text(
            'User',
            style: TextStyle(fontSize: 21.0),
          ),
          UserData(post: post),
          CommentsWidget(post: post)
        ],
      ),
    );
  }
}

class UserData extends StatelessWidget {
  const UserData({Key? key, required this.post}) : super(key: key);
  final PostsDomain post;
  @override
  Widget build(BuildContext context) {
    const style= TextStyle(fontSize: 18.0);
    return BlocBuilder<ZemogaBloc, ZemogaState>(
      buildWhen: (_, state) => state is UploadedGetUserByIdState,
      builder: (context, state) {
        if (state is UploadedGetUserByIdState) {
          var user = state.model.userDomain;

          return Container(
            margin: const EdgeInsets.only(top: 21.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Name:  ${user!.name}',style:style,),
                const SizedBox(height: 21.0),
                Text('Email:  ${user.email}',style: style,),
                const SizedBox(height: 21.0),
                Text('Phone:  ${user.phone}',style: style,),
                const SizedBox(height: 21.0),
                Text('Website:  ${user.website}',style: style,),
              ],
            ),
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}

class CommentsWidget extends StatelessWidget {
  const CommentsWidget({
    Key? key,
    required this.post,
  }) : super(key: key);
  final PostsDomain post;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 16.0),
            color: Colors.grey,
            child: Row(
              children: const <Widget>[
                Text(
                  'Comments',
                  style: TextStyle(fontSize: 21.0),
                ),
              ],
            ),
          ),
          const SizedBox(height: 21.0),
          Text(post.body!),
          const Text(
              'Cell description which explains the consequences of the\nabove actions.'),
          const Divider(height: 12.0, color: Colors.grey),
          const Text(
              'Cell description which explains the consequences of the\nabove actions.'),
          const Divider(height: 12.0, color: Colors.grey),
          const Text(
              'Cell description which explains the consequences of the\nabove actions.'),
          const Divider(height: 12.0, color: Colors.grey),
          const Text(
              'Cell description which explains the consequences of the\nabove actions.'),
          const Divider(height: 12.0, color: Colors.grey),
        ],
      ),
    );
  }
}
