import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_json_placeholder/src/app/presentation/bloc/zemoga_bloc.dart';

import '../views/content_posts_view.dart';

class PostsPage extends StatefulWidget {
  const PostsPage({Key? key}) : super(key: key);

  @override
  State<PostsPage> createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  bool isall = false;
  bool isfavorite = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Posts',
          style: TextStyle(
            color: Colors.white.withOpacity(0.8),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              context.read<ZemogaBloc>().add(FindAllPostsEvent());
            },
            icon: const Icon(
              Icons.update_sharp,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: allAndFavortesContainer(context),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        child: const Icon(Icons.delete_forever),
        onPressed: () {
          context.read<ZemogaBloc>().add(DeleteAllPostsEvent());
        },
      ),
    );
  }

  Container allAndFavortesContainer(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 21.0),
      child: Column(
        children: <Widget>[
          Container(
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              border: Border.all(color: Colors.green),
            ),
            child: Row(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    context.read<ZemogaBloc>().add(FindAllPostsEvent());
                    setState(() {
                      isall = true;
                      isfavorite = false;
                    });
                  },
                  child: Container(
                    width: 200,
                    decoration: BoxDecoration(
                      color: Colors.green[700]!.withOpacity(0.9),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(12.0),
                        bottomLeft: Radius.circular(12.0),
                      ),
                    ),
                    child: const Center(
                      child: Text(
                        'All',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      //  context
                      //     .read<ZemogaBloc>()
                      //     .add(FindAllPostsEvent());
                      context
                          .read<ZemogaBloc>()
                          .add(ShowAllFavoritesPostsFromDbEvent());

                      setState(() {
                        isfavorite = true;
                        isall = false;
                      });
                    },
                    child: Center(
                      child: Text(
                        'Favorites',
                        style:
                            TextStyle(fontSize: 18.0, color: Colors.green[700]),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          isfavorite
              ? const Expanded(child: FavoritesPostsList())
              : const Expanded(child: PostsList()),
        ],
      ),
    );
  }
}
