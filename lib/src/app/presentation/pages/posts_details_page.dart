import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_json_placeholder/src/app/presentation/bloc/zemoga_bloc.dart';

import '../../domain/entities/entities.dart';
import '../views/content_details_view.dart';

class PostsDetailsPage extends StatelessWidget {
  const PostsDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PostsDomain post =
        ModalRoute.of(context)!.settings.arguments as PostsDomain;

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Post',
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () {
            context.read<ZemogaBloc>().add(UpdatePostsListEvent());
            Navigator.pop(context);
          },
        ),
        actions: [
          BlocBuilder<ZemogaBloc, ZemogaState>(
              buildWhen: (_, state) => state is AddedToDbFavoritePostsState,
              builder: (context, state) {
                return IconButton(
                  onPressed: () {
                    if (!(state.model.favoritesPostsList!.contains(post))) {
                      post.isfavorite = 1;
                      context
                          .read<ZemogaBloc>()
                          .add(AddFavoritePostsToDbEvent(addFavorites: post));
                    } else {
                      return;
                    }
                  },
                  icon: const Icon(
                    Icons.star_border_outlined,
                    color: Colors.white,
                  ),
                );
              }),
        ],
      ),
      body: const ContentDetailsView(),
    );
  }
}
