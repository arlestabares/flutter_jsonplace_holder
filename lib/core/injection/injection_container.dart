import 'dart:async';

import 'package:flutter_json_placeholder/src/app/data/datasources/local/floor/dao/post_dao.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/floor/dao/user_dao.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/floor/db/database.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/local/local_data_source.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/remote/remote_datasource.dart';
import 'package:flutter_json_placeholder/src/app/data/datasources/remote/services/web_service_api.dart';
import 'package:flutter_json_placeholder/src/app/domain/usecases/delete_favorite_post_usecase.dart';
import 'package:flutter_json_placeholder/src/app/domain/usecases/delete_posts_by_id_usecase.dart';
import 'package:flutter_json_placeholder/src/app/domain/usecases/find_all_favorites_posts_usecase.dart';
import 'package:flutter_json_placeholder/src/app/domain/usecases/usecases.dart';
import 'package:flutter_json_placeholder/src/app/presentation/bloc/zemoga_bloc.dart';
import 'package:get_it/get_it.dart';

import '../../src/app/data/repositories/repository_impl.dart';
import '../../src/app/domain/repositories/domain_repository.dart';

final sl = GetIt.instance;
Future<void> init() async {
  final database =
      await $FloorAppDatabase.databaseBuilder('database.db').build();
  sl.registerSingleton<AppDatabase>(database);
  sl.registerSingleton<UserDao>(database.userDao);
  sl.registerSingleton<PostDao>(database.posDao);
  //Register WebServiceApi
  sl.registerFactory(() => WebServiceApi());

  //Factory register ZemogaBloc
  sl.registerFactory(
    () => ZemogaBloc(
      findPostsUseCase: sl(),
      getUserByIdUseCase: sl(),
      updatePostsListUseCase: sl(),
      addFavoritesPostsToDbUseCase: sl(),
      deleteAllPostsUseCase: sl(),
      deleteFavoritePostUseCase: sl(),
      findAllFavoritesPostsFromDbUsecase: sl(),
      deletePostsByIdUseCase: sl(),
    ),
  );

  //Register Usecase
  sl.registerLazySingleton(() => AddFavoritesPostsToDbUseCase(sl()));
  sl.registerLazySingleton(() => FindPostsUseCase(sl()));
  sl.registerLazySingleton(() => FindUserByIdUseCase(sl()));
  sl.registerLazySingleton(() => FindAllFavoritesPostsUseCase(sl()));
  sl.registerLazySingleton(() => UpdatePostsListUseCase(sl()));
  sl.registerLazySingleton(() => DeleteAllFavoritesPostsUseCase(sl()));
  sl.registerLazySingleton(() => DeleteAllPostsUseCase(sl()));
  sl.registerLazySingleton(() => DeletePostsByIdUseCase(sl()));
  sl.registerLazySingleton(() => DeleteFavoritePostUseCase(sl()));

  //Register repositories
  sl.registerLazySingleton<DomainRepository>(() => RepositoryImpl(
        remoteDataSource: sl(),
        localDataSource: sl(),
      ));

  //Register DataSources
  sl.registerLazySingleton<RemoteDataSource>(
      () => RemoteDataSourceImpl(webServiceApi: sl()));
  sl.registerLazySingleton<LocalDataSource>(
    () => LocalDataSourceImpl(postDao: sl(), userDao: sl()),
  );
  //External
}
