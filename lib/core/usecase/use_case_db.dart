
import 'package:equatable/equatable.dart';

abstract class UseCaseCoreDb<T, Params> {
  Future<T> call(Params params);
}

class Params extends Equatable {
  @override
  List<Object?> get props => [];
}
