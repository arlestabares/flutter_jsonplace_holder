import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_json_placeholder/core/errors/failure.dart';

abstract class IUseCaseCore<T, Params> {
  Future<Either<Failure, T>> call(Params param);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}