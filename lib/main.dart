import 'package:flutter/material.dart';
import 'package:flutter_json_placeholder/core/injection/injection_container.dart'
    as di;
import 'package:flutter_json_placeholder/src/blocs/multi_bloc_provider.dart';
import 'package:flutter_json_placeholder/src/routes/routes.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProviderWidget(
      child: MaterialApp(
        theme: ThemeData(
          appBarTheme: AppBarTheme(
            backgroundColor: Colors.green[700]!.withOpacity(0.95),
          ),
          useMaterial3: true,
        ),
        title: 'Material App',
        initialRoute: 'json_place_holder_page',
        routes: appRoutes,
      ),
    );
  }
}
