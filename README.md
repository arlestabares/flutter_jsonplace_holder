# flutter_json_placeholder

A new Flutter project.

## Getting Started
Proyecto que implementa Arquitectura por capas, Data, Domain y presentacion, tiene como gestor de estados BLOc y como localizador de servicios a get_it, 
comunicando la App con la data mediante casos de uso.
Cuenta con gestor local de almacenamiento Floor comunicando la app con la data almacenada mediante abstracciones.
Tambien tiene una version de Sqlite administrada por Sqflite envuelta en un  contenedor  reactivo llamado sqlbrite que en esta version de la app no se dejo implementada, en la siguiente version se podra tomar dicha opcion de almacenamiento de Sqlite administrada por sqflite. 
Se implementa Flutter 3.0.5  channel stable y version de Dart 2.17.6 

